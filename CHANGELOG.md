
# Changelog for VRE Manager Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v6.3.0] - 2021-10-13

- Ported to git

- Removed HomeLibrary dependency


## [v5-1-0] - 2015-12-11

- Fix for Incident #1511 - deactivating window.alert deactivates commit facility - Need for internal dialog

- Fix for Incident #1828 - Reports Manager of production contacting the dev RSG Webservice

- Added info alert shpwing to which Reports Store Gateway is connected in case of VME-DB

## [v4-8-0] - 2014-02-10

- Implemented Support for FAO Vulnerable Marine Ecosystem (VME) Update Scenario

- Implemented Support for FAO Vulnerable Marine Ecosystem (VME) Delete Scenario

- Added possibility of Deletion of content of the first entry

- Close report on commit when in FAO Vulnerable Marine Ecosystem (VME) Mode

## [v4-7-0] - 2013-12-19

- Added Support for FAO Vulnerable Marine Ecosystem (VME) Scenario

## [v4-2-0] - 2013-04-19

- Lots of improvements in the UI
- Embedded images in reports

## [v3-4-0] - 2012-05-04

- adapted to new Workspace Tree and home Library
