package org.gcube.portlets.user.reportgenerator.server.servlet;


public class ImagesUtil {
	public static final String GIF = "image/gif";
	public static final String PNG = "image/png";
	public static final String JPEG = "image/jpeg";
	public static final String JPG = "image/jpg";	
	public static final String TIFF = "image/tiff";
	public static final String BMP = "image/bmp";
	/**
	 * return a string for the file extension given a mimetype
	 * 
	 * @param bi the basketItem 
	 * @return a string for the file extension given a mimetype
	 */
	public static String getImageExtension() {

		String mimetype = "";
	

		if (mimetype.equals(GIF))
			return "gif";
		else if (mimetype.equals(PNG))
			return "png";
		else if (mimetype.equals(JPG))			
			return "jpg";
		else if (mimetype.equals(JPEG))
			return "jpg";
		else if (mimetype.equals(TIFF))
			return "png";
		else if (mimetype.equals(BMP))
			return "bmp";
		else 
			return "jpg";
	}
}
