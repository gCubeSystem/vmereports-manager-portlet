package org.gcube.portlets.user.reportgenerator.client.events;

import com.google.gwt.event.shared.EventHandler;

public interface AddBiblioEventHandler extends EventHandler {
	 void onAddCitation(AddBiblioEvent event);
}
