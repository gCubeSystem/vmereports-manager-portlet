package org.gcube.portlets.user.reportgenerator.server.servlet;

import java.io.IOException;
import java.io.StringReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

/**
 * Servlet implementation class DownloadImageServlet
 */
public class DownloadImageServlet extends HttpServlet {
	protected static Logger _log = Logger.getLogger(DownloadImageServlet.class);
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DownloadImageServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		resp.getWriter().write("Not supported");
		return;

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	}

	


	protected void sendError(HttpServletResponse response, String resultMessage) throws IOException	{	
		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		_log.trace("error message: "+resultMessage);
		_log.trace("writing response...");
		StringReader sr = new StringReader(resultMessage);
		IOUtils.copy(sr, response.getOutputStream());
		_log.trace("response wrote");
		response.flushBuffer();
	}

}
